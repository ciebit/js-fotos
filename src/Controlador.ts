import { Album } from './Album';
import { Foto } from './Foto';
import { Visualizador } from './Visualizador';

export class Controlador
{
    private Album:Album;
    private Visualizador:Visualizador;

    public constructor(Album:Album, Visualizador:Visualizador)
    {
        this.Album = Album;
        this.Visualizador = Visualizador;

        this.interligar(Album, Visualizador);
    }

    private atualizarSituacaoBotoes()
    {
        let fotoAnterior = this.Album.obterFotoAnterior();
        this.Visualizador.btAnteriorAtivo(!! fotoAnterior);

        let fotoProxima = this.Album.obterFotoProxima();
        this.Visualizador.btProximoAtivo(!! fotoProxima);
    }

    private interligar(Album:Album, Visualizador:Visualizador)
    {
        Album.aviseMe('foto-selecao', (Album:Album, Foto:Foto) => {
            this.transferirParaVisualizador(Foto);
            Visualizador.abrir();
        });

        Visualizador.aviseMe('foto-anterior', () => {
            let Foto = Album.obterFotoAnterior();
            Foto.selecionar();
            this.transferirParaVisualizador(Foto);
        });

        Visualizador.aviseMe('foto-proxima', () => {
            let Foto = Album.obterFotoProxima();
            Foto.selecionar();
            this.transferirParaVisualizador(Foto);
        });
    }

    public obterAlbum(): Album
    {
        return this.Album;
    }

    public obterVisualizador(): Visualizador
    {
        return this.Visualizador;
    }

    private transferirParaVisualizador(Foto:Foto): this
    {
        this.Visualizador.definirFoto(Foto);
        this.atualizarSituacaoBotoes();
        return this;
    }
}
