import { Foto } from './Foto';
import Hermes from '@ciebit/hermes';

export class Visualizador
{
    private classeAtivo:string;
    private classeInativo:string;
    private static contador:number = 0;

    private dadosDescricao:HTMLElement;
    private dadosImgAlt:HTMLElement;
    private dadosImgUrl:HTMLElement;
    private dadosTitulo:HTMLElement;

    private Documento:Document;

    private domAreaBts:HTMLElement;
    private domAreaImagem:HTMLElement;
    private domAreaSobre:HTMLElement;
    private domBtAnterior:HTMLButtonElement;
    private domBtFechar:HTMLButtonElement;
    private domBtProximo:HTMLButtonElement;
    private domDescricao:HTMLElement;
    private domImagem:HTMLImageElement;
    private domJanela:HTMLElement;
    private domTitulo:HTMLElement;

    private readonly eventos = {
        'fotoAnterior': 'foto-anterior',
        'fotoCarregada': 'foto-carregada',
        'fotoProxima': 'foto-proxima',
        'fotoSolicitada': 'foto-solicitada',
        'janelaAberta': 'janela-aberta',
        'janelaFechada': 'janela-fechada'
    }

    private Hermes:Hermes;

    private id:number;
    private instalado:boolean;
    private legenda:boolean;
    private referenciaFuncaoAtalhos:EventListenerObject;

    public constructor(
        Documento:Document,
        domJanela:HTMLElement,
        domAreaImagem:HTMLElement,
        domAreaBts:HTMLElement,
        domAreaSobre:HTMLElement,
        domImagem:HTMLImageElement,
        domBtAnterior:HTMLButtonElement,
        domBtProximo:HTMLButtonElement,
        domBtFechar:HTMLButtonElement,
        domDescricao:HTMLElement,
        domTitulo:HTMLElement
    ) {
        this.classeAtivo = 'cb-foto-visualizador-aberto';
        this.classeInativo = 'cb-foto-visualizador-fechado';

        this.Documento = Documento;
        this.domAreaBts = domAreaBts;
        this.domAreaImagem = domAreaImagem;
        this.domAreaSobre = domAreaSobre;
        this.domBtAnterior = domBtAnterior;
        this.domBtFechar = domBtFechar;
        this.domBtProximo = domBtProximo;
        this.domDescricao = domDescricao;
        this.domImagem = domImagem;
        this.domJanela = domJanela;
        this.domTitulo = domTitulo;

        this.id = Visualizador.contador++;
        this.Hermes = new Hermes;
        this.legenda = false;

        this.referenciaFuncaoAtalhos = this.capturarAtalhos.bind(this);

        this.fechar();
        this.atrelarEventos();
    }

    /*
    * Exibir o visualizar
    */
    public abrir():this
    {
        this.domJanela.classList.remove(this.classeInativo);
        this.domJanela.classList.add(this.classeAtivo);
        this.ativarAtalhos();
        this.Hermes.avise(this.eventos.janelaAberta, this);
        return this;
    }

    public ativarAtalhos(): this
    {
        // Adicionando evento as setas do teclado e tecla esc
        this.Documento.addEventListener('keyup', this.referenciaFuncaoAtalhos);

        return this;
    }

    /*
    * Atrela eventos
    */
    public aviseMe(evento:string, funcao:Function): this
    {
        this.Hermes.aviseMe(evento, funcao);
        return this;
    };

    /*
    * Define o estado do botão voltar
    */
    public btAnteriorAtivo(sit:boolean): this
    {
        this.domBtAnterior.disabled = !sit;
        return this;
    }

    /*
    * Define o estado do botão avançar
    */
    public btProximoAtivo(sit:boolean): this
    {
        this.domBtProximo.disabled = !sit;
        return this;
    }

    /*
    * Define a visibilidade dos botões de navegação
    */
    public botoesNavegacao(sit:boolean): this
    {
        this.domBtAnterior.hidden =
        this.domBtProximo.hidden = !sit;
        return this;
    }

    private capturarAtalhos(Evento:KeyboardEvent): this
    {
        switch (Evento.key) {
            case 'ArrowLeft':
                this.Hermes.avise(this.eventos.fotoAnterior);
                break;
            case 'ArrowRight':
                this.Hermes.avise(this.eventos.fotoProxima);
                break;
            case 'Escape':
                this.fechar();
                break;
        }

        return this;
    }

    /*
    * Função:  Define o valor da descrição
    * Recebe:  STRING com a descrição
    * Retorna: O Dom da descrição
    * Efeito:  Altera o DOM
    */
    public definirDescricao(descricao:string): this
    {
        this.domDescricao.innerHTML = descricao;
        return this;
    }

    public definirFoto(Foto:Foto): this
    {
        if (this.domImagem.getAttribute('src') == Foto.obterUrl()) {
            this.Hermes.avise(this.eventos.fotoCarregada, Foto);
            return this;
        }

        // Informando definição de imagem
        this.Hermes.avise(this.eventos.fotoSolicitada, Foto);

        this.domImagem.setAttribute('src', Foto.obterUrl());
        this.domImagem.setAttribute('alt', Foto.obterTextoAlternativo());

        return this;
    }

    /*
    * Função:  Define o valor do título
    * Recebe:  STRING com o título
    * Retorna: O próprio objeto
    * Efeito:  Altera o DOM
    */
    public definirTitulo(titulo:string): this
    {
        this.domTitulo.innerHTML = titulo;
        return this;
    }

    public desativarAtalhos(): this
    {
        this.Documento.removeEventListener('keyup', this.referenciaFuncaoAtalhos);
        return this;
    }

    /*
    * Oculta o visualizar
    */
    public fechar():this
    {
        this.domJanela.classList.remove(this.classeAtivo);
        this.domJanela.classList.add(this.classeInativo);
        this.Hermes.avise(this.eventos.janelaFechada);
        return this;
    }

    /*
    * Criar elementos no DOM e atrela eventos
    */
    private atrelarEventos(): this
    {
        // Adicionando evento para clique fora da foto
        this.domAreaImagem.addEventListener('click', (Evento) => {
            if (this.domAreaImagem == Evento.target) {
                this.fechar();
            }
        });

        //Adicionando evento aos botões
        this.domBtFechar.addEventListener('click', () => {
            this.fechar();
            this.Hermes.avise(this.eventos.janelaFechada);
        });

        this.domBtProximo.addEventListener('click', () => {
            this.Hermes.avise(this.eventos.fotoProxima);
        });

        this.domBtAnterior.addEventListener('click', () => {
            this.Hermes.avise(this.eventos.fotoAnterior);
        });

        //Adicionando evento a imagem
        // Informando carregamento da imagem
        this.domImagem.addEventListener('load', () => {
            this.Hermes.avise(this.eventos.fotoCarregada);
        });

        //Informa que foi instalado
        this.instalado = true;

        return this;
    }
}
