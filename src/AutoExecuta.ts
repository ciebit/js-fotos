import { Album } from './Album';
import { Controlador } from './Controlador';
import { Foto } from './Foto';
import { Visualizador } from './Visualizador';
import { VisualizadorFabrica } from './VisualizadorFabrica';

export class AutoExecuta
{
    private Albuns:Array<Album>;
    private construido:boolean;
    private Controladores:Array<Controlador>;
    private Documento:Document;
    private seletorAlbum:string;
    private seletorFoto:string;
    private Visualizador:Visualizador;

    public constructor(Documento:Document)
    {
        if (this.construido) {
            throw new Error('Só é permitido uma única instância de Ciebit.Fotos.AutoExecuta');
        }

        this.Albuns = [];
        this.Controladores = [];

        this.Documento = Documento;

        this.seletorAlbum = '.cb-fotos-album';
        this.seletorFoto = '.cb-fotos-foto';

        this.Documento.addEventListener('DOMContentLoaded', () => this.ligar());

        this.construido = true;
    }

    private criarVisualizador(Documento:Document): Visualizador
    {
        return VisualizadorFabrica.fabricar(Documento);
    }

    private coletarAlbuns(seletor:string): Array<Album>
    {
        let AlbunsDom = <NodeListOf<HTMLElement>> this.Documento.querySelectorAll(seletor);
        let total = AlbunsDom.length;
        let Albuns:Array<Album> = [];

        for (let indice = 0; indice < total; indice++) {
            Albuns.push(new Album(this.coletarFotos(AlbunsDom[indice])));
        }

        return Albuns;
    }

    private coletarFotos(Elemento:HTMLElement): Array<Foto>
    {
        let FotosDom = <NodeListOf<HTMLAnchorElement>>Elemento.querySelectorAll(this.seletorFoto);
        let total = FotosDom.length;
        let Fotos:Array<Foto> = [];

        for (let indice = 0; indice < total; indice++) {
            Fotos.push(new Foto(FotosDom[indice]));
        }

        return Fotos;
    }

    public definirSeletorAlbum(seletor:string): this
    {
        this.seletorAlbum = seletor;
        return this;
    }

    public definirSeletorFoto(seletor:string): this
    {
        this.seletorFoto = seletor;
        return this;
    }

    private ligar(): this
    {
        this.Albuns = this.coletarAlbuns(this.seletorAlbum);

        this.Albuns.forEach((Album) => {
            this.Controladores.push(new Controlador(Album, this.criarVisualizador(this.Documento)));
        });

        return this;
    }
}
