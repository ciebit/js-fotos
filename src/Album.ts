import { Foto } from './Foto';
import Hermes from '@ciebit/hermes';

export class Album
{
    public readonly ModosSelecao = {
        'unico': 1,
        'multiplo': 2
    }

    private static contador = 1;
    private fotos:Array<Foto>;
    private fotosSelecionadas:Array<Foto>;
    private Hermes: Hermes;
    private id:number;
    private modoSelecao: number;
    private titulo:string;

    public constructor(fotos:Array<Foto>)
    {
        this.fotos = fotos;
        this.fotosSelecionadas = [];
        this.Hermes = new Hermes;
        this.id = Album.contador++;
        this.modoSelecao = this.ModosSelecao.unico;

        this.fotos.forEach((Foto) => {
            this.observarFoto(Foto);
        });
    }

    public adicionarFoto(Foto:Foto): this
    {
        this.observarFoto(Foto);
        this.fotos.push(Foto);
        return this;
    }

    public aviseMe(acao:string, funcao:Function): this
    {
        this.Hermes.aviseMe(acao, funcao);
        return this;
    }

    public definirModoSelecao(modo:number): this
    {
        for (let chave in this.ModosSelecao) {
            if (this.ModosSelecao[chave] == modo) {
                this.modoSelecao = modo;
                return this;
            }
        }

        throw new Error('Modo de seleção inválido');
    }

    public eventoSelecao(Foto:Foto): this
    {
        if (this.modoSelecao == this.ModosSelecao.unico) {
            this.removerSelecaoFotos(Foto);
        }

        this.fotosSelecionadas.push(Foto);
        this.Hermes.avise('foto-selecao', this, Foto);

        return this;
    }

    private observarFoto(Foto:Foto): this
    {
        Foto.aviseMe('selecionada', (Foto:Foto) => this.eventoSelecao(Foto));

        return this;
    }

    public obterFotoAnterior(): Foto
    {
        let UltimaSelecao = this.obterUltimaSelecao();

        if (! UltimaSelecao) {
            return;
        }

        let indice = this.fotos.indexOf(UltimaSelecao);

        return this.fotos[indice - 1];
    }

    public obterFotoProxima(): Foto
    {
        let UltimaSelecao = this.obterUltimaSelecao();

        if (! UltimaSelecao) {
            return;
        }

        let indice = this.fotos.indexOf(UltimaSelecao);

        return this.fotos[indice + 1];
    }

    public obterUltimaSelecao(): Foto
    {
        return this.fotosSelecionadas[this.fotosSelecionadas.length -1];
    }

    public obterTotalFotos(): number
    {
        return this.fotos.length;
    }

    private removerSelecaoFotos(FotoExcessao:Foto): this
    {
        this.fotosSelecionadas.forEach((Foto) => {
            if (Foto.obterId() != FotoExcessao.obterId()) {
                Foto.removerSelecao();
            }
        });

        return this;
    }
}
