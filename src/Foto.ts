import Hermes from '@ciebit/hermes';

export class Foto
{
    private static contador = 1;
    private id:number;
    private Alvo:HTMLAnchorElement;
    private classeSelecao:string;
    private Hermes:Hermes;

    public constructor(Alvo:HTMLAnchorElement)
    {
        this.Alvo = Alvo;
        this.classeSelecao = 'cb-fotos-foto_selecionada';
        this.id = Foto.contador++;
        this.Hermes = new Hermes;

        this.atrelarEvento();
    }

    private atrelarEvento(): this
    {
        this.Alvo.addEventListener('click', (Evento) => {
            Evento.preventDefault();
            this.selecionar();
        });

        return this;
    }

    public aviseMe(acao:string, funcao:Function): this
    {
        this.Hermes.aviseMe(acao, funcao);
        return this;
    }

    public definirClasseSelecao(classe:string): this
    {
        this.classeSelecao = classe;
        return this;
    }

    public obterClasseSelecao(classe:string): string
    {
        return this.classeSelecao;
    }

    public obterId(): number
    {
        return this.id;
    }

    public obterTextoAlternativo(): string
    {
        return this.Alvo.getAttribute('alt');
    }

    public obterUrl(): string
    {
        return this.Alvo.getAttribute('href');
    }

    public selecionar(): this
    {
        this.Alvo.classList.add(this.classeSelecao);
        this.Hermes.avise('selecionada', this);
        return this;
    }

    public removerSelecao(): this
    {
        this.Alvo.classList.remove(this.classeSelecao);
        this.Hermes.avise('selecao-removida', this);
        return this;
    }
}
