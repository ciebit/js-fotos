import { Visualizador } from './Visualizador';

export class VisualizadorFabrica
{
    public static fabricar(Documento:Document): Visualizador
    {
        //Criando elementos
        let domJanela:HTMLElement = Documento.createElement('div');
        let domAreaImagem:HTMLElement = Documento.createElement('div');
        let domAreaSobre:HTMLElement = Documento.createElement('div');
        let domAreaBts:HTMLElement = Documento.createElement('div');
        let domImagem:HTMLImageElement = Documento.createElement('img');
        let domBtFechar:HTMLButtonElement = Documento.createElement('button');
        let domBtAnterior:HTMLButtonElement = Documento.createElement('button');
        let domBtProximo:HTMLButtonElement = Documento.createElement('button');
        let domTitulo:HTMLElement = Documento.createElement('h1');
        let domDescricao:HTMLElement = Documento.createElement('p');

        //Adicionando ID
        domJanela.className = 'cb-foto-visualizador-janela';
        domAreaImagem.className = 'cb-foto-visualizador-area_imagem';
        domAreaSobre.className = 'cb-foto-visualizador-area_sobre';
        domAreaBts.className = 'cb-foto-visualizador-area_bts';
        domImagem.className = 'cb-foto-visualizador-imagem';
        domBtFechar.className = 'cb-foto-visualizador-bt_fechar';
        domBtAnterior.className = 'cb-foto-visualizador-bt_anterior';
        domBtProximo.className = 'cb-foto-visualizador-bt_proximo';
        domTitulo.className = 'cb-foto-visualizador-titulo';
        domDescricao.className = 'cb-foto-visualizador-descricao';

        //Adicionado Atributos
        domBtFechar.title   = 'Fechar';
        domBtAnterior.title = 'Imagem Anterior';
        domBtProximo.title  = 'Próxima Imagem';

        //Adicionado classe e texto nos botões
        domBtFechar.className   += ' cb-foto-visualizador-bt';
        domBtAnterior.className += ' cb-foto-visualizador-bt';
        domBtProximo.className  += ' cb-foto-visualizador-bt';

        domBtFechar.appendChild(Documento.createTextNode('x'));
        domBtAnterior.appendChild(Documento.createTextNode('<'));
        domBtProximo.appendChild(Documento.createTextNode('>'));

        //Inserindo elementos na Janela
        domJanela.appendChild(domAreaImagem);
        domJanela.appendChild(domAreaSobre);
        domJanela.appendChild(domAreaBts);

        //Inserindo elementos na Área de Imagem
        domAreaImagem.appendChild(domImagem);

        //Inserindo botões
        domAreaBts.appendChild(domBtFechar);
        domAreaBts.appendChild(domBtAnterior);
        domAreaBts.appendChild(domBtProximo);

        //Inserindo elementos na Área Sobre
        domAreaSobre.appendChild(domTitulo);
        domAreaSobre.appendChild(domDescricao);

        //Inserindo janela na página
        Documento.body.appendChild(domJanela);

        return new Visualizador(
            Documento,
            domJanela,
            domAreaImagem,
            domAreaBts,
            domAreaSobre,
            domImagem,
            domBtAnterior,
            domBtProximo,
            domBtFechar,
            domDescricao,
            domTitulo
        );
    }
}
