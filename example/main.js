requirejs.config({
    paths: {
        "@ciebit/hermes": "http://js.ciebit.com/hermes/v3.0.3",
        "@ciebit/fotos": "../dist/js/fotos"
    }
});

require(['@ciebit/fotos'], function(Photos){
    var Visualizador = Photos.VisualizadorFabrica.fabricar(document);
    var photosCollection = [];
    var links = document.querySelectorAll('a');

    Array.prototype.forEach.call(links, function(link){
        photosCollection.push(new Photos.Foto(link));
    });
    var Albun = new Photos.Album(photosCollection);
    var Controller = new Photos.Controlador(Albun, Visualizador);
});
